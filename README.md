# 基于jquery的拼图游戏

#### 项目介绍
有加奇偶矩阵判断的，可调节难度的，可切换图片，基于jquery 的一个拼图小游戏demo
 
在线体验：
https://aqiong.top/pintu.html

[点击在线体验](https://aqiong.top/pintu.html)（图片加载可能会比较慢 稍等几秒就会出现）

示列
https://pan.baidu.com/s/17OpnlMt0Y_wwCEOiz64CrQ
[运行图片示列](https://pan.baidu.com/s/17OpnlMt0Y_wwCEOiz64CrQ)
![输入图片说明](https://aqiong.top/demo.jpg "在这里输入图片标题")
#### 安装教程

1. 直接下载即可使用，如果有帮助请点个star。